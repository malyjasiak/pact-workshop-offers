package com.jmalyjasiak.pactworkshop.offer.client;

import au.com.dius.pact.consumer.dsl.PactBuilder;
import au.com.dius.pact.consumer.dsl.PactDslJsonBody;
import au.com.dius.pact.consumer.junit.MockServerConfig;
import au.com.dius.pact.consumer.junit5.PactConsumerTest;
import au.com.dius.pact.consumer.junit5.PactTestFor;
import au.com.dius.pact.core.model.PactSpecVersion;
import au.com.dius.pact.core.model.V4Pact;
import au.com.dius.pact.core.model.annotations.Pact;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Map;

@SpringBootTest
@Tag("pact-consumer-test")
@PactConsumerTest
@PactTestFor(providerName = "promotions", pactVersion = PactSpecVersion.V4)
@MockServerConfig(port = "8080", providerName = "promotions")
@Disabled
public class UglyManyPromotionsContractTest {

    @Autowired
    PromotionsClient promotionsClient;

    @Pact(consumer = "offers", provider = "promotions")
    public V4Pact defineGetAllPromotionsPact(PactBuilder builder) {
        return builder.expectsToReceiveHttpInteraction(
                "a get request for a promotion",
                interaction -> interaction
                        .state("cash promotion exists", Map.of("promotionId", "some-cash-promotion"))
                        .state("percent promotion exists", Map.of("promotionId", "some-percent-promotion"))
                        .withRequest(request -> request
                                .path("/v1/promotions")
                                .method("GET"))
                        .willRespondWith(response -> response
                                .headers(Map.of("Content-Type", "application/json"))
                                .status(200)
                                .body(new PactDslJsonBody()
                                        .arrayContaining("promotions")
                                            .object()
                                                .stringType("promotionId", "some-promotion-id")
                                                .object("reward", new PactDslJsonBody()
                                                        .stringValue("type", "CASH")
                                                        .object("discount", new PactDslJsonBody()
                                                                .decimalType("amount", new BigDecimal("10.00"))
                                                                .stringMatcher("currency", "^[A-Z]{3}$", "USD")
                                                                .asBody())
                                                        .asBody())
                                            .closeObject()
                                            .object()
                                                .stringType("promotionId", "some-promotion-id")
                                                .object("reward", new PactDslJsonBody()
                                                        .stringValue("type", "PERCENT")
                                                        .decimalType("discount", new BigDecimal("50"))
                                                        .asBody())
                                            .closeObject()
                                        .closeArray()
                                        .asBody())
                        )
        ).toPact();
    }

    @Test
    void shouldReturnPromotionGivenPromotionId() {
        var allPromotions = promotionsClient.getAllPromotions();
        System.out.println(allPromotions);
    }
}
