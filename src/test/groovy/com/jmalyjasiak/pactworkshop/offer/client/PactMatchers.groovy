package com.jmalyjasiak.pactworkshop.offer.client

import au.com.dius.pact.consumer.groovy.Matcher
import au.com.dius.pact.consumer.groovy.Matchers

class PactMatchers {

    static final Matcher positiveMoneyAmount(String value) {
        Matchers.regexp(~/^\d+(\.\d+)?$/, value)
    }

    static final Matcher currencyCode(String value) {
        Matchers.regexp(~/^[A-Z]{3}$/, value)
    }

    static final Closure money(String moneyString) {
        var amountAndCurrency = moneyString.split(' ')
        return {
            amount PactMatchers.positiveMoneyAmount(amountAndCurrency[0])
            currency PactMatchers.currencyCode(amountAndCurrency[1])
        }
    }
}
