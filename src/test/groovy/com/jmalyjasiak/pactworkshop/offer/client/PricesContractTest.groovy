package com.jmalyjasiak.pactworkshop.offer.client


import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

import static com.jmalyjasiak.pactworkshop.offer.client.PactMatchers.currencyCode
import static com.jmalyjasiak.pactworkshop.offer.client.PactMatchers.positiveMoneyAmount

class PricesContractTest extends ConsumerContractTest {

    @Autowired
    PricesClient pricesClient

    PricesContractTest() {
        super('prices')
    }

    @Test
    void "Should return all prices"() {
        pact.with {
            given('product price exists', ['productId': 'some-product-id'])
            uponReceiving 'a get request for a promotion'
            withAttributes(
                    method: 'GET',
                    path: '/v1/prices'
            )
            willRespondWith(
                    status: 200,
                    headers: [
                            'Content-Type': 'application/json'
                    ]
            )
            withBody {
                productId string('some-product-id')
                price {
                    amount positiveMoneyAmount('10.00')
                    currency currencyCode('USD')
                }
                unit regexp(~/^EACH|PER_100G$/, 'EACH')
            }
            runTestAndVerify {
                pricesClient.getAllPrices()
            }
        }
    }
}