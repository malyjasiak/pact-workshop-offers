package com.jmalyjasiak.pactworkshop.offer.client


import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

import java.time.Instant

import static com.jmalyjasiak.pactworkshop.offer.client.PactMatchers.currencyCode
import static com.jmalyjasiak.pactworkshop.offer.client.PactMatchers.positiveMoneyAmount
import static org.assertj.core.api.Assertions.assertThat

class PromotionsContractTest extends ConsumerContractTest {

    @Autowired
    PromotionsClient promotionsClient

    PromotionsContractTest() {
        super('wro-jug')
    }

    @Test
    void "Should return promotion given promotion id"() {
        pact.with {
            given('cash promotion exists', ['promotionId': 'some-promotion-id'])
            uponReceiving 'a get request for a promotion'
            withAttributes(
                    method: 'GET',
                    path: '/v1/promotions/some-promotion-id'
            )
            willRespondWith(
                    status: 200,
                    headers: [
                            'Content-Type': 'application/json'
                    ]
            )
            withBody {
                promotionId string('some-promotion-id')
                activeFrom datetime('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'', '2023-01-01T12:00:00Z')
                reward {
                    type 'CASH'
                    discount {
                        amount positiveMoneyAmount('5.00')
                        currency currencyCode('USD')
                    }
                }
            }
            runTestAndVerify {
                var promotion = promotionsClient.getPromotion("some-promotion-id")

                assertThat(promotion.promotionId()).isEqualTo("some-promotion-id")
                assertThat(promotion.activeFrom()).isEqualTo(Instant.parse("2023-01-01T12:00:00Z"))
                assertThat(promotion.reward()).isInstanceOf(PromotionsClient.CashReward.class)

                var reward = (PromotionsClient.CashReward) promotion.reward()
                assertThat(reward.discount()).isNotNull()
                assertThat(reward.discount().amount()).isEqualTo(new BigDecimal("5.00"))
                assertThat(reward.discount().currency()).isEqualTo("USD")
            }
        }
    }
}