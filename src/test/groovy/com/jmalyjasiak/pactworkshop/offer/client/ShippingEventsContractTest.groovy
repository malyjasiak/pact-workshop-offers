package com.jmalyjasiak.pactworkshop.offer.client

import au.com.dius.pact.consumer.groovy.messaging.PactMessageBuilder
import au.com.dius.pact.core.model.messaging.MessageInteraction
import com.fasterxml.jackson.databind.ObjectMapper
import com.jmalyjasiak.pactworkshop.offer.event.ShipmentEventHandler
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

import static au.com.dius.pact.core.model.PactSpecVersion.V4

@SpringBootTest
@Tag('pact-consumer-test')
class ShippingEventsContractTest {

    @Autowired
    ObjectMapper objectMapper

    @Autowired
    ShipmentEventHandler shipmentEventHandler

    @Test
    void "Should handle shipment DELIVERED event"() {
        new PactMessageBuilder(V4).call {
            serviceConsumer "offers"
            hasPactWith "shipments"
            expectsToReceive "shipment delivered event"
            withMetaData([tenantId: 'tenant-1'])
            withContent(contentType: 'application/json') {
                type 'SHIPMENT_DELIVERED'
                shipmentId string('shipment-1')
            }
            run { MessageInteraction message ->
                def shipmentEvent = objectMapper.readValue(message.contentsAsString(), ShipmentEventHandler.ShipmentEvent.class)
                shipmentEventHandler.handleEvent(shipmentEvent, message.metadata)
            }
        }
    }
}