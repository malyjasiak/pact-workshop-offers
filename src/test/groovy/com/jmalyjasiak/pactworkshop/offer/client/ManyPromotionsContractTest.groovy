package com.jmalyjasiak.pactworkshop.offer.client


import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

import static com.jmalyjasiak.pactworkshop.offer.client.PactMatchers.currencyCode
import static com.jmalyjasiak.pactworkshop.offer.client.PactMatchers.positiveMoneyAmount

class ManyPromotionsContractTest extends ConsumerContractTest {

    @Autowired
    PromotionsClient promotionsClient

    ManyPromotionsContractTest() {
        super('promotions')
    }

    @Test
    void "Should return all promotions"() {
        pact.with {
            given('cash promotion exists', ['promotionId': 'some-cash-promotion'])
            given('percent promotion exists', ['promotionId': 'some-percent-promotion'])
            uponReceiving 'a get request for all promotions'
            withAttributes(
                    method: 'GET',
                    path: '/v1/promotions'
            )
            willRespondWith(
                    status: 200,
                    headers: [
                            'Content-Type': 'application/json'
                    ]
            )
            withBody {
                promotions arrayContaining([
                        {
                            promotionId string('some-cash-promotion')
                            reward {
                                type 'CASH'
                                discount {
                                    amount positiveMoneyAmount('10.00')
                                    currency currencyCode('USD')
                                }
                            }
                        }, {
                            promotionId string('some-percent-promotion')
                            reward {
                                type 'PERCENT'
                                discount positiveMoneyAmount('50')
                            }
                        }])
            }

            runTestAndVerify {
                var promotions = promotionsClient.getAllPromotions()
                // assertions go there
                println promotions
            }
        }
    }
}