package com.jmalyjasiak.pactworkshop.offer.client

import au.com.dius.pact.consumer.groovy.PactBuilder
import org.junit.jupiter.api.Tag
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@Tag('pact-consumer-test')
class ConsumerContractTest {

    final PactBuilder pact

    ConsumerContractTest(String providerName) {
        pact = new PactBuilder()
                .port(8080)
                .serviceConsumer('offers')
                .hasPactWith(providerName)
    }
}