package com.jmalyjasiak.pactworkshop.offer.client

import au.com.dius.pact.consumer.dsl.*
import au.com.dius.pact.consumer.junit.MockServerConfig
import au.com.dius.pact.consumer.junit5.PactConsumerTest
import au.com.dius.pact.consumer.junit5.PactTestFor
import au.com.dius.pact.core.model.PactSpecVersion
import au.com.dius.pact.core.model.V4Pact
import au.com.dius.pact.core.model.annotations.Pact
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.math.BigDecimal

@SpringBootTest
@Tag("pact-consumer-test")
@PactConsumerTest
@PactTestFor(providerName = "promotions", pactVersion = PactSpecVersion.V4)
@MockServerConfig(port = "8080", providerName = "promotions")
@Disabled
object KotlinLambdaPromotionsContractTest {

    @Autowired
    var promotionsClient: PromotionsClient? = null

    @Pact(consumer = "offers", provider = "promotions")
    fun defineGetPromotionPact(builder: PactBuilder): V4Pact? {
        return builder.expectsToReceiveHttpInteraction("a get request for all promotions") { interaction ->
            interaction
                    .state("cash promotion exists", mapOf(Pair("promotionId", "some-promotion-id")))
                    .withRequest {
                        it.method("GET").path("/v1/promotions/some-promotion-id")
                    }
                    .willRespondWith {
                        it.status(200)
                                .header("Content-Type", "application/json")
                                .body(newJsonObject {
                                    stringType("promotionId", "some-promotionId")
                                    newObject("reward") {
                                        stringValue("type", "CASH")
                                        newObject("discount") {
                                            decimalType("amount", BigDecimal("10.00"))
                                            stringMatcher("currency", "^[A-Z]{3}$", "USD")
                                        }
                                    }
                                })
                    }
        }.toPact()
    }

    @Test
    fun shouldReturnPromotionGivenPromotionId() {
        val result = promotionsClient!!.getPromotion("some-promotion-id")
        println(result)
    }
}