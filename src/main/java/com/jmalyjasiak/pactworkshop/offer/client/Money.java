package com.jmalyjasiak.pactworkshop.offer.client;

import java.math.BigDecimal;

public record Money(String currency, BigDecimal amount) {
}
