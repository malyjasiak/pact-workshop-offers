package com.jmalyjasiak.pactworkshop.offer.client;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@FeignClient("promotions")
public interface PromotionsClient {

    @GetMapping("/v1/promotions")
    Promotions getAllPromotions();

    @GetMapping("/v1/promotions/{promotionId}")
    Promotion getPromotion(@PathVariable("promotionId") String promotionId);

    record Promotion(
            String promotionId,
            Instant activeFrom,
            Reward reward
    ) {
    }

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, visible = true, property = "type")
    @JsonSubTypes(value = {
            @JsonSubTypes.Type(value = CashReward.class, name = "CASH"),
            @JsonSubTypes.Type(value = PercentReward.class, name = "PERCENT")
    })
    interface Reward {
        String type();
    }

    record CashReward(
            Money discount
    ) implements Reward {
        @Override
        public String type() {
            return "CASH";
        }
    }

    record PercentReward(
            BigDecimal discount
    ) implements Reward {
        @Override
        public String type() {
            return "PERCENT";
        }
    }

    record Promotions(
            List<Promotion> promotions
    ) {

    }
}
