package com.jmalyjasiak.pactworkshop.offer.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient("prices")
public interface PricesClient {

    @GetMapping("/v1/prices")
    Prices getAllPrices();

    record Prices(
            List<Price> prices
    ) {}

    record Price(
            String productId,
            Unit unit,
            Money price
    ) {}

    enum Unit {
        EACH, PER_100G
    }
}
