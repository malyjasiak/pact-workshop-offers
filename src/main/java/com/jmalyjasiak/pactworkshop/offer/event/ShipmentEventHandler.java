package com.jmalyjasiak.pactworkshop.offer.event;

import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class ShipmentEventHandler {

    public void handleEvent(ShipmentEvent shipmentEvent, Map<String, Object> metadata) {
        System.out.println("SHIPMENT EVENT HANDLED");
    }

    public record ShipmentEvent(String type, String shipmentId) {}
}
